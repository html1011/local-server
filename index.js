const net = require('net'),
http = require("http"),
fs = require("fs");
// Keep track of the chat clients
var clients = [];

// Start a TCP Server
var socket = net.createServer(function (socket) {
    clients.push(socket);
    var port = [3030, 8080, 3000, 2000],
    i = 0;
    socket.write("Listening at port " + port[i] + "\n");
    var ser = http.createServer(function(req, res) {
        if(req.url == "/") {
            fs.readFile("./index.html", function(err, data) {
                if(err) throw err;
                res.write(data);
                res.end();
            });
        }
        if(req.method == "POST") {
            var fixingUp = req.url.split("/").join("");
            if(!socket.name) {
            socket.name = fixingUp;
            socket.write("Logged in as " + socket.name + "\n");
            console.log(socket.name + " joined the conversation.");
            clients.forEach(function (val, ind) {
                if (val.name !== socket.name) {
                    val.write(socket.name + " joined the con\
                    versation.\n");
                }
            });
        }
        else {
            console.log(socket.name + " said: " + fixingUp.split("%20").join(" "));
            clients.forEach(function (val, ind) {
                if (val.name !== socket.name) {
                    val.write(socket.name + " said: " + fixingUp.split("%20").join(" ") + "\n");
                }
            });
        }
    }
    }).on("error", function() {
        i++;
        socket.write("Hmmm... Try " + port[i] + " instead.");
    })
    .listen(port[i], "localhost");
    // Handle incoming messages from clients.
    socket.on('data', function (data) {
        var ans = data.toString().trim();
        if (!socket.name) {
            socket.name = ans;
            name = ans;
            socket.write("You're all set! Start typing!\n");
            clients.forEach(function (val, ind) {
                if (val.name !== socket.name) {
                    val.write(socket.name + " joined the conversation.");
                }
            });
            console.log(socket.name + " joined the conversation.");
        }
        else {
            if (ans !== "") {
                console.log(socket.name + " said: " + ans);
                clients.forEach(function (val, index) {
                    val.write(socket.name + " said: " + ans + "\n");
                });
            }
        }
    });
    // Remove the client from the list when it leaves
    socket.on('end', function () {
        console.log(socket.name + " left the conversation.");
        clients.splice(clients.indexOf(socket), 1);
        clients.forEach(function(val, ind) {
            val.write(socket.name + " left the conversation. Don't worry, " + (clients.length - 1 > 0 ? (clients.length + " people are here.\n") : "you're all alone!\n"));
        });
    });
}).listen(5000, "192.168.1.188");
// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 5000\n");